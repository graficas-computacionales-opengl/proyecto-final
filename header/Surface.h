#pragma once

#ifdef __APPLE__
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
	#include <GLUT/glut.h>
#else
	#include "freeglut.h"
#endif
#include "cTexture.h"

#ifndef __SURFACE
#define __SURFACE

class Surface
{
public:
			Surface	(	float	side,
						bool	use_mipmaps	);
			~Surface	(	void				);

	void	draw	(	void				);

private:
	float	side;
	float	hside;
	Texture targas[1];
};

#endif __SURFACE
