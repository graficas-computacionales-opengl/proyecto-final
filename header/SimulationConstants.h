#ifndef Point_hpp
#define Point_hpp

#include <stdio.h>
#endif /* Point_hpp */
#pragma once
#ifdef __APPLE__
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
    #include <GLUT/glut.h>
#else
    #include "freeglut.h"
#endif
#include "Bezier.h"

#define MAX_B_CURVES 40

class SimulationConstants {
public:
    SimulationConstants();
    ~SimulationConstants();

    void curve1();
    void curve2();
    void curve3();
    void curve4();
    void curve5();
    void curve6();
    void curve7();
    void curve8();
    void curve9();
    void curve10();
    void curve11();
    void curve12();
    void curve13();
    void curve14();
    void curve15();
    void curve16();
    void curve17();
    void curve18();
    void curve19();
    void curve20();
    void curve21();
    void curve22();
    void curve23();
    void curve24();
    void curve25();
    void curve26();
    void curve27();
    void curve28();
    void curve29();
    void curve30();
    void curve31();
    void curve32();
    void curve33();
    void curve34();
    void curve35();
    void curve36();
    void curve37();
    void curve38();
    void curve39();
    void curve40();
    void curve41();
    void curve42();
    void curve43();
    void curve44();
    void curve45();

    void init();
    Bezier* bezierCurves[MAX_B_CURVES];
};
