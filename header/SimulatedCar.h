//
// Wheel.cpp
//
// David Alexis Zarate Trujillo
// Eduardo Valencia Paz
// César David Betancourt Adame
// Julio
// David
//

#pragma once
#ifdef __APPLE__
// See: http://lnx.cx/docs/opengl-in-xcode/
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
    #include <GLUT/glut.h>
#else
    #include "freeglut.h"
#endif
#include <stdio.h>
#include <math.h>

#ifndef GLM_H
  #include "glm.h"
  #define GLM_H 5
#endif

#include "Bezier.h"
#include "Point.h"
#include "Wheel.h"

#ifndef NUMWHEELS
    #define NUMWHEELS 4
#endif

#ifndef simcarh
#define simcarh

class Car;

class SimulatedCar {
public:
    SimulatedCar(float _x, float _y, float _z, Bezier* bezier, float _tRatio);
    SimulatedCar(float _x, float _y, float _z, Bezier* bezier, float _tRatio, GLMmodel * _car, GLMmodel * wheel);
    SimulatedCar();
    ~SimulatedCar();

    void draw();
    void update();
    void moveCar(float direction, float _direction2);
    void moveWithCurve(float _x, float _z);
    bool inCollision(Car* car);
    bool inCollision(SimulatedCar* car);

    float x, y, z;
    Wheel * wheels[NUMWHEELS];
    GLMmodel *car;
    float car_dims[3];
    float posX;
    float posY;
    float posZ;
    float rot;
    float rotStep;
    float vx;
    float vz;
    float currentT;
    float tRatio;
    Bezier* bezier;
    float futureX;
    float futureZ;
    float radius;
    int collisionPriority;
    bool shouldMove;
};

#endif
