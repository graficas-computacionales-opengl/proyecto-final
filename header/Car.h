//
// Wheel.cpp
//
// David Alexis Zarate Trujillo
// Eduardo Valencia Paz
// César David Betancourt Adame
// Julio
// David
//

#pragma once
#ifdef __APPLE__
// See: http://lnx.cx/docs/opengl-in-xcode/
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
    #include <GLUT/glut.h>
#else
    #include "freeglut.h"
#endif
#include <stdio.h>
#include <math.h>

#ifndef GLM_H
  #include "glm.h"
  #define GLM_H 5
#endif

#include "Wheel.h"
#ifndef NUMWHEELS
    #define NUMWHEELS 4
#endif

#ifndef carh
#define carh

class SimulatedCar;

class Car{
public:
    Car(float _x, float _y, float _z, bool _cam);
    Car();
    ~Car();

    void draw();
    void update();
    void moveCar(float direction, float _direction2, SimulatedCar* cars[], int carsLength);
    void moveWithCurve(float _x, float _z);
    bool willCollide(SimulatedCar* car);
    bool inCollision(SimulatedCar* car);
    void changeCamera(bool _change);

    float x, y, z;
    Wheel * wheels[NUMWHEELS];
    GLMmodel *car;
    float car_dims[3];
    float posX;
    float posY;
    float posZ;
    float rot;
    float rotStep;
    float vx;
    float vz;

    float radius;
    int collisionPriority;
    bool shouldMove;
    float futureX;
    float futureZ;

    bool cameraShouldFollow;
};

#endif
