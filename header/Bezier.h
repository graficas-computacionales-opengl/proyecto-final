//
//  Bezier.h
//  Bezier Curves
//
//  Created by Eduardo Valencia on 4/12/18.
//  Copyright © 2018 Eduardo Valencia. All rights reserved.
//

#ifndef Bezier_hpp
#define Bezier_hpp

#include <stdio.h>
#include <math.h>
#include "Point.h"

#endif /* Bezier_hpp */

#pragma once
#ifdef __APPLE__
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
    #include <GLUT/glut.h>
#else
    #include "freeglut.h"
#endif
// Add includes for drawing

class Bezier {
public:
    Bezier(int deg, Point* points);
    Bezier();
    ~Bezier();
    long int factorial(int x);
    float binomial(int n, int i);
    Point* evalBezier(float t);
    void draw();
    void update();
    int evalPoints = 20;

    Point* ctrlPoints;
    int degree;
    float* coefficients;
    float currentT;
};
