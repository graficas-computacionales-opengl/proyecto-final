//
// Wheel.cpp
//
// David Alexis Zarate Trujillo
// Eduardo Valencia Paz
// César David Betancourt Adame
// Julio
// David
//

#pragma once
#ifdef __APPLE__
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
	#include <GLUT/glut.h>
#else
	#include "freeglut.h"
#endif

#ifndef GLM_H
  #include "glm.h"
  #define GLM_H 5
#endif

class Wheel {
public:
  Wheel(float _x, float _y, float _z);
  Wheel(float _x, float _y, float _z, GLMmodel*	_wheel);
  ~Wheel();

  void draw();
  void update();
  void resetWheelSteering();
  void rotate(float angle);
  void rotForward(float angle);
  void rotBackwards(float angle);

  float x;
  float y;
  float z;

  int rotationAngleX;
  float rotationAngleY;
  bool wheelShouldGoRight;

  GLMmodel*	wheel;

  float wheel_dims[3];
};
