//
// Wheel.cpp
//
// David Alexis Zarate Trujillo
// Eduardo Valencia Paz
// César David Betancourt Adame
// Julio
// David
//

#pragma once
#ifdef __APPLE__
// See: http://lnx.cx/docs/opengl-in-xcode/
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
    #include <GLUT/glut.h>
#else
    #include "freeglut.h"
#endif
#include <stdio.h>
#include <math.h>

#ifndef GLM_H
  #include "glm.h"
  #define GLM_H 5
#endif

class Building{
public:
    Building(float _x, float _y, float _z, char* _n, float _s, float _r);
    ~Building();

    void draw();
    void update();

    float x, y, z;
    GLMmodel *building;
    float building_dims[3];
    float posX;
    float posY;
    float posZ;
    float size;
    float rot;
};
