//
// Wheel.cpp
//
// David Alexis Zarate Trujillo
// Eduardo Valencia Paz
// César David Betancourt Adame
// Julio
// David
//

#pragma once
#ifdef __APPLE__
// See: http://lnx.cx/docs/opengl-in-xcode/
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
    #include <GLUT/glut.h>
#else
    #include "freeglut.h"
#endif
#include <stdio.h>
#include <math.h>

#ifndef GLM_H
  #include "glm.h"
  #define GLM_H 5
#endif

class Plane{
public:
    Plane(float _x, float _z, char* _n, float _s);
    ~Plane();

    void draw();
    void update();

    float x, z;
    GLMmodel *plane;
    float plane_dims[3];
    float posX;
    float posZ;
    float size;
};
