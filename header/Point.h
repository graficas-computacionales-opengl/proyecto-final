//
//  Point.h
//  Bezier Curves
//
//  Created by Eduardo Valencia on 4/12/18.
//  Copyright © 2018 Eduardo Valencia. All rights reserved.
//

#ifndef Point_hpp
#define Point_hpp

#include <stdio.h>
#endif /* Point_hpp */
#pragma once
#ifdef __APPLE__
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
    #include <GLUT/glut.h>
#else
    #include "freeglut.h"
#endif

class Point {
public:
    Point();
    Point(float _x, float _y, float _z, float _r, float _g, float _b, float _radius);
    ~Point();

    void draw();

    float x, y, z;
    float r, g, b;
    float radius;
};
