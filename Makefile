main: source/main.cpp glmimg.o glm.o Wheel.o Car.o cTexture.o Surface.o Building.o Plane.o Bezier.o Point.o SimulatedCar.o SimulationConstants.o Camera.o vecs.o
	@g++ -o $@ source/main.cpp glmimg.o glm.o Wheel.o Car.o cTexture.o Surface.o Building.o Plane.o Bezier.o Point.o SimulatedCar.o SimulationConstants.o Camera.o vecs.o -I header/ -framework OpenGL -framework GLUT -w
	rm -f glmimg.o glm.o cTexture.o Wheel.o Car.o Surface.o Building.o Plane.o Bezier.o Point.o SimulatedCar.o SimulationConstants.o Camera.o vecs.o

Surface.o: source/Surface.cpp cTexture.o
	@g++ -o $@ -c source/Surface.cpp -I header/ -framework OpenGL -framework GLUT -w

Plane.o: source/Plane.cpp glm.o
	@g++ -o $@ -c source/Plane.cpp -I header/ -framework OpenGL -framework GLUT -w

Building.o: source/Building.cpp glm.o
	@g++ -o $@ -c source/Building.cpp -I header/ -framework OpenGL -framework GLUT -w

Car.o: source/Car.cpp glm.o
	@g++ -o $@ -c source/Car.cpp -I header/ -framework OpenGL -framework GLUT -w

Wheel.o: source/Wheel.cpp glm.o
	@g++ -o $@ -c source/Wheel.cpp -I header/ -framework OpenGL -framework GLUT -w

glmimg.o: source/glmimg.cpp glm.o
	@g++ -o $@ -c source/glmimg.cpp -I header/ -framework OpenGL -framework GLUT -w

glm.o: source/glm.cpp
	@g++ -o $@ -c source/glm.cpp -I header/ -framework OpenGL -framework GLUT -w

cTexture.o: source/cTexture.cpp
	@g++ -o $@ -c source/cTexture.cpp -I header/ -framework OpenGL -framework GLUT -w

Camera.o: source/Camera.cpp vecs.o
	@g++ -o $@ -c source/Camera.cpp -I header/ -framework OpenGL -framework GLUT -w

vecs.o: source/vecs.cpp
	@g++ -o $@ -c source/vecs.cpp -I header/ -framework OpenGL -framework GLUT -w

Bezier.o: source/Bezier.cpp Point.o
	@g++ -o $@ -c source/Bezier.cpp -I header/ -framework OpenGL -framework GLUT -w

Point.o: source/Point.cpp
	@g++ -o $@ -c source/Point.cpp -I header/ -framework OpenGL -framework GLUT -w

SimulatedCar.o: source/SimulatedCar.cpp Car.o
	@g++ -o $@ -c source/SimulatedCar.cpp -I header/ -framework OpenGL -framework GLUT -w

SimulationConstants.o: source/SimulationConstants.cpp Car.o
	@g++ -o $@ -c source/SimulationConstants.cpp -I header/ -framework OpenGL -framework GLUT -w

clean:
	rm -f main glmimg.o glm.o cTexture.o Wheel.o Car.o Surface.o Building.o Plane.o Bezier.o Point.o SimulatedCar.o SimulationConstants.o
