//
// Wheel.cpp
//
// David Alexis Zarate Trujillo
// Eduardo Valencia Paz
// César David Betancourt Adame
// Julio
// David
//

#include "SimulatedCar.h"
#include "Car.h"

SimulatedCar::SimulatedCar() {

}

SimulatedCar::SimulatedCar(float _x, float _y, float _z, Bezier* _bezier, float _tRatio, GLMmodel * _car, GLMmodel* _wheel) {
    shouldMove = true;
    tRatio = _tRatio;
    currentT = 0;
    bezier = _bezier;
    car = _car;

    for (int i = 0; i < NUMWHEELS; i++) {
        wheels[i] = new Wheel(0,0,0, _wheel);
    }
    wheels[0]->x = -0.37;
    wheels[1]->x = -0.37;
    wheels[0]->y = -0.4;
    wheels[1]->y = -0.4;
    wheels[0]->z = 0.65;
    wheels[1]->z = -0.55;
    wheels[2]->x = 0.37;
    wheels[3]->x = 0.37;
    wheels[2]->y = -0.4;
    wheels[3]->y = -0.4;
    wheels[2]->z = 0.65;
    wheels[3]->z = -0.55;
    posX = _x;
    posY = _y;
    posZ = _z;
    rot = 0.0f;
    rotStep = 10.0f;
    vx = 0.0f;
    vz = 1.0f;
    Point* point = bezier->evalBezier(currentT);
    futureX = point->x;
    futureZ = point->z;
    radius = 1.2f;
}

SimulatedCar::SimulatedCar(float _x, float _y, float _z, Bezier* _bezier, float _tRatio) {
    shouldMove = true;
    tRatio = _tRatio;
    currentT = 0;
    bezier = _bezier;
    car = glmReadOBJ("assets/Car_2/Car_2.obj");
    glmUnitize(car);
    glmFacetNormals(car);
    glmDimensions(car, car_dims);

    for (int i = 0; i < NUMWHEELS; i++) {
        wheels[i] = new Wheel(0,0,0);
    }
    wheels[0]->x = -0.37;
    wheels[1]->x = -0.37;
    wheels[0]->y = -0.4;
    wheels[1]->y = -0.4;
    wheels[0]->z = 0.65;
    wheels[1]->z = -0.55;
    wheels[2]->x = 0.37;
    wheels[3]->x = 0.37;
    wheels[2]->y = -0.4;
    wheels[3]->y = -0.4;
    wheels[2]->z = 0.65;
    wheels[3]->z = -0.55;
    posX = _x;
    posY = _y;
    posZ = _z;
    rot = 0.0f;
    rotStep = 10.0f;
    vx = 0.0f;
    vz = 1.0f;
    Point* point = bezier->evalBezier(currentT);
    futureX = point->x;
    futureZ = point->z;
    radius = 1.2f;
}

SimulatedCar::~SimulatedCar(){

}

void SimulatedCar::moveWithCurve(float _x, float _z) {
  posX = _x;
  posZ = _z;
}

void SimulatedCar::draw(){
    //bezier->draw();
    glPushMatrix();
    {
        glTranslatef(posX, posY, posZ);
        glPushMatrix();
        {
            glRotatef(rot, 0, 1.0, 0);
            glScalef(1.5f, 1.5f, 1.5f);
            glmDraw(car, GLM_SMOOTH | GLM_TEXTURE);
            //glutSolidSphere(1.1f, 10, 10); // Radius sphere
            for (int i = 0; i < NUMWHEELS; i++) {
                wheels[i]->draw();
            }
        }
        glPopMatrix();
    }
    glPopMatrix();
}

void SimulatedCar::update(){
    if(shouldMove) {
      currentT += tRatio;
      if(currentT >= 1.0f) {
        //currentT = 0.0f;
        shouldMove = false;
      }

      posX = futureX;
      posZ = futureZ;

      Point* point = bezier->evalBezier(currentT);
      futureX = point->x;
      futureZ = point->z;
    }

    float vX = futureX - posX;
    float vZ = futureZ - posZ;

    float vMagnitude = sqrt(pow(vX, 2) + pow(vZ, 2));
    rot = ((atan2(vX,vZ) * 180.0f) / M_PI);

    /*glLoadIdentity();
    gluLookAt(posX-7.0, posY+15.0, posZ+5.0,
		posX, posY, posZ,
		0.0, 1.0, 0.0);*/
}

bool SimulatedCar::inCollision(Car* car) {
  float r1 = radius;
  float r2 = car->radius;
  float x1 = posX;
  float x2 = car->posX;
  float z1 = posZ;
  float z2 = car->posZ;

  float dx2 = (x2-x1)*(x2-x1);
  float dz2 = (z2-z1)*(z2-z1);
  float dist = dx2 + dz2;
  float sumR = r1 + r2;
  float sumR2 = pow(sumR, 2);
  if(dist < sumR2) {
    return true;
  }
  return false;
}

bool SimulatedCar::inCollision(SimulatedCar* car) {
  float r1 = radius;
  float r2 = car->radius;
  float x1 = posX;
  float x2 = car->posX;
  float z1 = posZ;
  float z2 = car->posZ;

  float dx2 = (x2-x1)*(x2-x1);
  float dz2 = (z2-z1)*(z2-z1);
  float dist = dx2 + dz2;
  float sumR = r1 + r2;
  float sumR2 = pow(sumR, 2);
  if(dist < sumR2) {
    return true;
  }
  return false;
}
