#include "Surface.h"

Surface::Surface(float side, bool use_mipmaps) {
    this->side = side;
    hside = side / 2.0f;

    glGenTextures(1, &targas[0].texID);
    glBindTexture(GL_TEXTURE_2D, targas[0].texID);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    if(use_mipmaps) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    } else {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    LoadTGA(&targas[0], "assets/textures/texture_floor.tga");

    if(use_mipmaps) {
        gluBuild2DMipmaps(
            GL_TEXTURE_2D,
            3,
            targas[0].width,
            targas[0].height,
            GL_RGB,
            GL_UNSIGNED_BYTE,
            targas[0].imageData);
    } else {
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RGB,
            targas[0].width,
            targas[0].height,
            0,
            GL_RGB,
            GL_UNSIGNED_BYTE,
            targas[0].imageData);
    }
    glBindTexture(GL_TEXTURE_2D, 0);
}

Surface::~Surface( void ) {
	glDeleteTextures( 1, &targas[0].texID );
}

void Surface::draw( void ) {
    glBindTexture(GL_TEXTURE_2D, targas[0].texID);
    glBegin(GL_QUADS); // TOP
	{
		// Bottom left
		glNormal3f(0.0f, 1.0f, 0.0f);
		glTexCoord2d(0.0f, 0.0f);
		glVertex3f(-hside, 0.0f, hside);

		// Bottom right
		glNormal3f(0.0f, 1.0f, 0.0f);
		glTexCoord2d(1.0f, 0.0f);
		glVertex3f(hside, 0.0f, hside);

		// Top right
		glNormal3f(0.0f, 1.0f, 0.0f);
		glTexCoord2d(1.0f, 1.0f);
		glVertex3f(hside, 0.0f, -hside);

		// Top left
		glNormal3f(0.0f, 1.0f, 0.0f);
		glTexCoord2d(0.0f, 1.0f);
		glVertex3f(-hside, 0.0f, -hside);

	}
	glEnd();
}

