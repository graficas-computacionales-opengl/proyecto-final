//
// Wheel.cpp
//
// David Alexis Zarate Trujillo
// Eduardo Valencia Paz
// César David Betancourt Adame
// Julio
// David
//

#include "Plane.h"

Plane::Plane(float _x, float _z, char* _n, float _s) {
    plane = glmReadOBJ(_n);
    glmUnitize(plane);
    glmFacetNormals(plane);
    glmDimensions(plane, plane_dims);

    posX = _x;
    posZ = _z;
    size = _s;
}

Plane::~Plane(){

}

void Plane::draw(){
    glPushMatrix();
    {
        glTranslatef(posX, -4.0f, posZ);
        glPushMatrix();
        {
            glScalef(size, 1.0f, size);
            glmDraw(plane, GLM_SMOOTH | GLM_TEXTURE);
        }
        glPopMatrix();
    }
    glPopMatrix();
}

void Plane::update(){
    
}