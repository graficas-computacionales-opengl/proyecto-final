/*
	David Alexis Zárate Trujillo A01332978
	Eduardo Valencia Paz A01333346
	Cesar David Betancourt Adame A01338883
	David Guillermo Legorreta Barba A01335334
	Julio Miguel. A. Enríquez Castillo A01335065﻿
*/

#ifdef __APPLE__
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
	#include <GLUT/glut.h>
#else
	#include "freeglut.h"
#endif

#include <stdio.h>
#include <math.h>
#include "Surface.h"
#include "Car.h"
#include "Building.h"
#include "Plane.h"
#include "Camera.h"
#include "Bezier.h"
#include "Point.h"
#include "SimulatedCar.h"
#include "SimulationConstants.h"

#ifndef GLM_H
  #include "glm.h"
  #define GLM_H 5
#endif

#define MAX_CARS 468
#define CURRENT_CARS 468
#define TEST_OFFSET 0

long		frames;
long		times;
long		timebase;
float		fps;
float		rotation;
GLfloat*	global_ambient;

// Car model
float car_model_dims[3];
float wheel_model_dims[3];
GLMmodel * carModel;
GLMmodel * wheelModel;

//Surface *surface;
Car *car;
bool arrows[4];
bool draggableCamera;
enum ARROWS{
	UP,
	DOWN,
	LEFT,
	RIGHT
};
Building *casa;
Building *iglu1;
Building *iglu2;
Building *iglu3;
Building *iglu4;
Building *iglu5;
Building *iglu6;
Building *iglu7;
Building *iglu8;
Building *iglu9;
Building *caseta;
Building *arbol;
Building *moon;
Plane *plane;

int mouseCoords[2], mouseMotionType = 0;
Camera sceneCam, lightCam;
Camera *draggedCamera, *currentCamera;

char bufferFPS[11];	// For on-screen text.

Bezier* bezier;
SimulatedCar* cars[MAX_CARS];

/*
	Display mode:
	0: Vertices.
	1: Flat.
	2: Smooth.
	3: Textured.
*/
int mode;

// Display text:
void displayText( int x, int y, char* txt );
void checkForCollisions();

void display( void )
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	if(draggableCamera){
		currentCamera->setView();
	}

	car->draw();

	// Draw sim cars
	for(int i = 0 + TEST_OFFSET; i < CURRENT_CARS; i++) {
		cars[i]->draw();
	}

	casa->draw();
	caseta->draw();

	iglu1->draw();
	iglu2->draw();
	iglu3->draw();
	iglu4->draw();
	iglu5->draw();
	iglu6->draw();
	iglu7->draw();
	iglu8->draw();
	iglu9->draw();

	arbol->draw();
	moon->draw();
	plane->draw();

	glPushMatrix();
	{
		displayText( 5, 20, bufferFPS );
	}
	glPopMatrix();

	glutSwapBuffers();
}

void idle( void )
{
	if(arrows[LEFT]^arrows[RIGHT]){
		if(arrows[LEFT]){
			if(arrows[UP]){
				car->moveCar(1,1, cars, MAX_CARS);
			}else if(arrows[DOWN]){
				car->moveCar(-1,-1, cars, MAX_CARS);
			}
		}if(arrows[RIGHT]){
			if(arrows[UP]){
				car->moveCar(-1,1, cars, MAX_CARS);
			}else if(arrows[DOWN]){
				car->moveCar(1,-1, cars, MAX_CARS);
			}
		}
	}else{
		if(arrows[UP]){
			car->moveCar(0,1, cars, MAX_CARS);
		}
		if(arrows[DOWN]){
			car->moveCar(0,-1, cars, MAX_CARS);
		}
	}

	// Move sim cars
	for(int i = 0 + TEST_OFFSET; i < CURRENT_CARS; i++) {
		cars[i]->update();
	}

	// Compute frames per second:
	frames++;
	times = glutGet( GLUT_ELAPSED_TIME );
	if( times - timebase > 1000 )
	{
		fps = frames * 1000.0f / (times - timebase);
		sprintf( bufferFPS, "FPS:%4.2f\n", fps);
		timebase = times;
		frames = 0;
	}
	checkForCollisions();
	car->update();
	glutPostRedisplay();
}

void reshape( int x, int y )
{
	glMatrixMode(GL_PROJECTION);									// Go to 2D mode.
	glLoadIdentity();												// Reset the 2D matrix.
	gluPerspective(40.0, (GLdouble)x / (GLdouble)y, 0.5, 450.0);		// Configure the camera lens aperture.
	glMatrixMode(GL_MODELVIEW);										// Go to 3D mode.
	glLoadIdentity();												// Reset the 3D matrix.
	glViewport(0, 0, x, y);											// Configure the camera frame dimensions.
	gluLookAt(1.0f, 330.0f, 0.0f,										// Where the camera is.
		0.0f, 0.0f, 0.0f,												// To where the camera points at.
		0.0, 1.0, 0.0);												// "UP" vector.
	display();
}

void init( void )
{
	frames			= 0;
	times			= 0;
	timebase		= 0;
	fps				= 0.0f;
	rotation		= 0.0f;
	mode			= 0;

	draggableCamera = false;

	carModel = glmReadOBJ("assets/Car_2/Car_2.obj");
	glmUnitize(carModel);
    glmFacetNormals(carModel);
    glmDimensions(carModel, car_model_dims);

	wheelModel = glmReadOBJ("assets/Wheel/Wheel.obj");
  	glmUnitize(wheelModel);
  	glmFacetNormals(wheelModel);
  	glmDimensions(wheelModel, wheel_model_dims);

	//surface = new Surface(222.452f, true);
	car = new Car(-5.0f, 1.0f, -22.0f, true);
	casa = new Building(-42.0f, 1.0f, 80.0f, "assets/Building_casa/Casa.obj", 24.0f, 210.0f);
	moon = new Building(-48.0f, 1.0f, 43.0f, "assets/Half_moon/Media_luna.obj", 13.0f, 200.0f);
	caseta = new Building(-90.0f, 1.0f, 37.0f, "assets/Building_caseta/Caseta.obj", 4.5f, 0.0f);

	iglu1 = new Building(-6.0f, 1.0f, -39.0f, "assets/Building_iglu/iglu.obj", 4.5f, 0.0f);
	iglu2 = new Building(-26.0f, 1.0f, -43.0f, "assets/Building_iglu/iglu.obj", 4.5f, 0.0f);
	iglu3 = new Building(-46.0f, 1.0f, -58.0f, "assets/Building_iglu/iglu.obj", 4.5f, 0.0f);
	iglu4 = new Building(-54.0f, 1.0f, -80.0f, "assets/Building_iglu/iglu.obj", 4.5f, 0.0f);
	iglu5 = new Building(-54.0f, 1.0f, -100.0f, "assets/Building_iglu/iglu.obj", 4.5f, 0.0f);
	iglu6 = new Building(14.0f, 1.0f, -43.0f, "assets/Building_iglu/iglu.obj", 4.5f, 0.0f);
	iglu7 = new Building(34.0f, 1.0f, -58.0f, "assets/Building_iglu/iglu.obj", 4.5f, 0.0f);
	iglu8 = new Building(42.0f, 1.0f, -80.0f, "assets/Building_iglu/iglu.obj", 4.5f, 0.0f);
	iglu9 = new Building(42.0f, 1.0f, -100.0f, "assets/Building_iglu/iglu.obj", 4.5f, 0.0f);

	arbol = new Building(-54.0f, 1.0f, -39.0f, "assets/Tree/arbol.obj", 4.5f, 0.0f);

	plane = new Plane(0.0f, 0.0f, "assets/Plane/plano.obj", 111.226f);

	sceneCam.setPos(112, 230, 112);
	sceneCam.setDirVec(-2,-6,-2.5);
	sceneCam.setPivot(0,0,0);
	sceneCam.fov = 45;
	sceneCam.near_plane = 1;
	sceneCam.far_plane = 1000;
	currentCamera = &sceneCam;

	glEnable( GL_DEPTH_TEST );
	glEnable( GL_TEXTURE_2D );

	glClearColor( 0.0, 0.0, 0.0, 0.0 );

	glEnable( GL_LIGHTING );
	glEnable( GL_LIGHT0 );

	GLfloat diffusel0[4]	= { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat ambientl0[4]	= { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat specularl0[4]	= { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat position[4]		= { 2.0f, 0.5f, 1.0f, 0.0f };
	glLightfv( GL_LIGHT0, GL_AMBIENT,   ambientl0  );
	glLightfv( GL_LIGHT0, GL_DIFFUSE,   diffusel0  );
	glLightfv( GL_LIGHT0, GL_SPECULAR,  specularl0 );
	glLightfv( GL_LIGHT0, GL_POSITION,  position   );

	global_ambient			= new GLfloat[4];
	global_ambient[0]		= 0.3f;
	global_ambient[1]		= 0.3f;
	global_ambient[2]		= 0.3f;
	global_ambient[3]		= 1.0f;
	glLightModelfv( GL_LIGHT_MODEL_AMBIENT, global_ambient );
	glLightModeli( GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE );
	glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE );

	SimulationConstants* simulationConstants = new SimulationConstants();

	// Curve 1
	for (int i = 0; i < 12; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[0];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z - (15.0f - (3.0f * i));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 2
	for (int i = 12; i < 24; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[1];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.0f - (3.0f * (i - 12)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 3
	for (int i = 24; i < 36; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[2];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.0f - (3.0f * (i - 24)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 4
	for (int i = 36; i < 48; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[3];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.0f - (3.0f * (i - 36)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}

		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 5
	for (int i = 48; i < 60; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[4];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.0f - (3.0f * (i - 48)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 6
	for (int i = 60; i < 69; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[5];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (12.5f - (3.0f * (i - 60)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 7
	for (int i = 69; i < 78; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[6];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (12.5f - (3.0f * (i - 69)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 8
	for (int i = 78; i < 89; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[7];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 78)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 9
	for (int i = 89; i < 100; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[8];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 89)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 10
	for (int i = 100; i < 110; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[9];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 100)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	
	// Curve 11 (Inclined)
	for (int i = 110; i < 119; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[10];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x - (24.0f - (3.0f * (i - 110)));
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z + (8.5f - (0.95 * (i - 110)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	

	// Curve 12
	for (int i = 119; i < 130; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[11];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 119)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 13
	for (int i = 130; i < 140; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[12];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z + (11.5f - (3.0f * (i - 130)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}
	

	// Curve 14
	for (int i = 140; i < 151; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[13];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i -140)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	
	
	// Curve 15
	for (int i = 151; i < 163; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[14];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i -151)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	
	// Curve 16 (Inclined)
	for (int i = 163; i < 172; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[15];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x - (23.8f - (3.0f * (i - 163)));
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z + (7.2f - (0.95 * (i - 163)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 17 
	for (int i = 172; i < 183; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[16];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 172)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 18
	for (int i = 183; i < 195; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[17];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z+ (16.7f - (3.0f * (i - 183)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 19
	for (int i = 195; i < 206; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[18];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 195)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}
	
	// Curve 20
	for (int i = 206; i < 220; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[19];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z+ (17.2f - (3.0f * (i - 206)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 21
	for (int i = 220; i < 231; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[20];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i -220)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 22
	for (int i = 231; i < 242; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[21];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 231)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 23
	cars[242] = new SimulatedCar(20.0f, 0.0f, 0.0f, simulationConstants->bezierCurves[22], 0.001f);

	// Curve 24
	for (int i = 243; i < 252; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[23];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z+ (11.7f - (3.0f * (i - 243)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 25
	for (int i = 252; i < 266; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[24];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z+ (17.2f - (3.0f * (i - 252)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 26
	for (int i = 266; i < 275; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[25];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z+ (11.75f - (3.0f * (i - 266)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 27
	for (int i = 275; i < 286; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[26];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 275)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 28
	for (int i = 286; i < 297; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[27];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 286)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 29
	for (int i = 297; i < 306; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[28];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z+ (11.0f - (3.0f * (i - 297)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	
	// Curve 30
	for (int i = 306; i < 315; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[29];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z+ (11.0f - (3.0f * (i - 306)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f,carModel, wheelModel);
	}

	// Curve 31
	for (int i = 315; i < 326; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[30];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 315)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 32
	for (int i = 326; i < 337; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[31];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 326)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 33
	for (int i = 337; i < 346; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[32];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z+ (11.0f - (3.0f * (i - 337)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 34
	for (int i = 346; i < 358; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[33];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z - (15.0f - (3.0f * (i - 346)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 35
	for (int i = 358; i < 369; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[34];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z - (15.0f - (3.0f * (i - 358)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 36
	for (int i = 369; i < 380; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[35];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z - (15.0f - (3.0f * (i - 369)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 37
	for (int i = 380; i < 392; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[36];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z - (15.0f - (3.0f * (i - 380)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 38
	for (int i = 392; i < 403; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[37];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z - (15.0f - (3.0f * (i - 392)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 39
	for (int i = 403; i < 413; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[38];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x - 3.0f;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z - (12.2f - (3.0f * (i - 403)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 40
	for (int i = 413; i < 424; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[39];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 413)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 41
	for (int i = 424; i < 435; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[40];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 424)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 42
	for (int i = 435; i < 446; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[41];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 435)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 43
	for (int i = 446; i < 457; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[42];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (15.5f - (3.0f * (i - 446)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}

	// Curve 44
	for (int i = 457; i < 468; i++) {
		Bezier* bezier = simulationConstants->bezierCurves[43];
		Point* currentPoints = bezier->ctrlPoints;
		Point* ctrlPoints = new Point[bezier->degree];
		for (int j = 0; j < bezier->degree; j++) {
			if(j >= ((bezier->degree) -4)){
				ctrlPoints[j].x = currentPoints[j].x + 0.5;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z- (16.5f - (3.0f * (i - 457)));
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			} else {
				ctrlPoints[j].x = currentPoints[j].x;
				ctrlPoints[j].y = currentPoints[j].y;
				ctrlPoints[j].z = currentPoints[j].z;
				ctrlPoints[j].r = currentPoints[j].r;
				ctrlPoints[j].g = currentPoints[j].g;
				ctrlPoints[j].b = currentPoints[j].b;
				ctrlPoints[j].radius = currentPoints[j].radius;
			}
		}
		Bezier* offsetBezier = new Bezier(bezier->degree, ctrlPoints);
		cars[i] = new SimulatedCar(20.0f, 0.0f, 0.0f, offsetBezier, 0.001f, carModel, wheelModel);
	}
	
	

	//cars[69] = new SimulatedCar(20.0f, 0.0f, 0.0f, simulationConstants->bezierCurves[6], 0.001f);
}

void mouse( int button, int state, int x, int y)
{
	int mods;
	mouseCoords[0] = x;
	mouseCoords[1] = y;

	if(state == GLUT_DOWN){
		if(button == GLUT_LEFT_BUTTON){
			draggedCamera = &sceneCam;
		} else if(button == GLUT_RIGHT_BUTTON){
			draggedCamera = &lightCam;
		} else {
			draggedCamera = 0;
		}
		mods = glutGetModifiers();
		if(mods & GLUT_ACTIVE_SHIFT){
			mouseMotionType = 2;
		} else if(mods & GLUT_ACTIVE_ALT){
			mouseMotionType = 3;
		} else if(mods & GLUT_ACTIVE_CTRL){
			mouseMotionType = 4;
		} else {
			mouseMotionType = 1;
		}
	} else {
		if(button == GLUT_LEFT_BUTTON)
			mouseMotionType = 0;
	}
}

void motion(int x, int y)
{
	if(!draggedCamera) return;
	if(mouseMotionType == 4) {
		//Type 4: Zoom
		draggedCamera->moveForward((mouseCoords[1] - y) * 0.05); // zTrans += (mouseCoords[1] - y) * 0.02;
		draggedCamera->rotate((mouseCoords[0] - x) * 0.2, 0, 1, 0);
		mouseCoords[0] = x;
		mouseCoords[1] = y;
	} else if(mouseMotionType == 2) {
		// Type 2: Translation on X / Y
		draggedCamera->rotate((mouseCoords[1] - y) * 0.1, 1, 0, 0); // xTrans += (x - mouseCoords[0]) * 0.02;
		draggedCamera->rotate((x - mouseCoords[0]) * 0.1, 0, 0, 1); // yTrans += (mouseCoords[1] - y) * 0.02;
		mouseCoords[0] = x;
		mouseCoords[1] = y;
	} else if(mouseMotionType == 3) {
		// Type 3: Tilt
		draggedCamera->moveUp((mouseCoords[1] - y) * 0.05);   // yTrans += (y - mouseCoords[1]) * 0.02;
		draggedCamera->moveLeft((mouseCoords[0] - x) * 0.05); // xTrans += (mouseCoords[0] - x) * 0.02;

		mouseCoords[0] = x;
		mouseCoords[1] = y;
	} else if(mouseMotionType == 1) {
		// Type 1: Rotate scene
		draggedCamera->moveAround((mouseCoords[1] - y) * 0.1, 1, 0, 0);   // yTrans += (y - mouseCoords[1]) * 0.02;
		draggedCamera->moveAround((mouseCoords[0] - x) * 0.1, 0, 1, 0); // xTrans += (mouseCoords[0] - x) * 0.02;

		mouseCoords[0] = x;
		mouseCoords[1] = y;
	}
}

void keyboard( unsigned char key, int x, int y )
{
	switch( key )
	{
	case '0':
		mode = 0;
		break;
	case '1':
		mode = 1;
		break;
	case '2':
		mode = 2;
		break;
	case '3':
		mode = 3;
		break;
	case 27:
		//glmDelete( sintel );
		//exit(0);
		break;
	}
}

void keys(unsigned char key, int x, int y){
	switch(key){
		case 'c':
			if(currentCamera == &sceneCam)
				currentCamera = &lightCam;
			else
				currentCamera = &sceneCam;
			break;
		case 'w':
				arrows[UP]=true;
			break;
		case 's':
				arrows[DOWN]=true;
			break;
		case 'a':
				arrows[LEFT]=true;
			break;
		case 'd':
				arrows[RIGHT]=true;
			break;
		case '7':
			draggableCamera = true;
			break;
		case '8':
			car->changeCamera(true);
			draggableCamera = false;
			break;
		case '9':
			car->changeCamera(false);
			draggableCamera = false;
			sceneCam.lookNew(1.0f, 330.0f, 0.0f,
				0.0f, 0.0f, 0.0f,
				0.0, 1.0, 0.0);
			break;
		case '0':
			car->changeCamera(false);
			draggableCamera = false;
			glLoadIdentity();
        	gluLookAt(-112.0f, 330.0f, 0.0f,
				0.0f, 0.0f, 0.0f,
				0.0, 1.0, 0.0);
			break;
	}
	glutPostRedisplay();
}
void keysUp(unsigned char key, int x, int y){
	switch(key){
		case 'w':
				arrows[UP]=false;
			break;
		case 's':
				arrows[DOWN]=false;
			break;
		case 'a':
				arrows[LEFT]=false;
			break;
		case 'd':
				arrows[RIGHT]=false;
			break;
	}
	glutPostRedisplay();
}

int main( int argc, char* argv[] )
{
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
	glutInitWindowSize( 800, 800 );
	glutInitWindowPosition( 100, 10 );
	glutCreateWindow( "Proyecto Final" );
	glutReshapeFunc( reshape );
	glutKeyboardFunc( keyboard );
	glutKeyboardFunc(keys);									// Keyboard CALLBACK function.
	glutKeyboardUpFunc(keysUp);
	init();
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutDisplayFunc( display );
	glutIdleFunc( idle );
	glutMainLoop();
	return 0;
}

void checkForCollisions()
{
	int collisions[MAX_CARS][MAX_CARS] = {{0}};

	for (int i = 0 + TEST_OFFSET; i < MAX_CARS; i++) {
		bool collided = false;
		int collidedWith = -1;
		for (int j = 0 + TEST_OFFSET; j < MAX_CARS && collided == false; j++) {
			if(i != j && cars[i]->inCollision(cars[j])) {
				collided = true;
				collidedWith = j;
			}
		}
		if(cars[i]->inCollision(car)) {
			cars[i]->shouldMove = false;
		} else if(collided) {
			if (i > collidedWith) {
				if(collidedWith >= 0 && cars[collidedWith]->currentT < 1.0) {
					cars[i]->shouldMove = false;
				} else {
					cars[i]->shouldMove = true;
				}
			} else {
				if (cars[i]->currentT < 1) {
					cars[i]->shouldMove = true;
				}
			}
		} else {
			if (cars[i]->currentT < 1) {
				cars[i]->shouldMove = true;
			}
		}
	}
}

/*
Displays characters stored in the "txt" array at position (x,y).
*/
void displayText( int x, int y, char* txt )
{
	GLboolean lighting;
	GLint viewportCoords[4];
	glColor3f( 0.0, 1.0, 0.0 );
	glGetBooleanv( GL_LIGHTING, &lighting	   );
	glGetIntegerv( GL_VIEWPORT, viewportCoords );
	if( lighting )
	{
		glDisable( GL_LIGHTING );
	}
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
		glLoadIdentity();
		gluOrtho2D( 0.0, viewportCoords[2], 0.0, viewportCoords[3] );
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
			glLoadIdentity();
			glRasterPos2i( x, viewportCoords[3] - y );
			while( *txt )
			{
				glutBitmapCharacter( GLUT_BITMAP_HELVETICA_18, *txt );
				txt++;
			}
		glPopMatrix();
		glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );

	if( lighting )
	{
		glEnable( GL_LIGHTING );
	}
}
