//
//  Point.cpp
//  Bezier Curves
//
//  Created by Eduardo Valencia on 4/12/18.
//  Copyright © 2018 Eduardo Valencia. All rights reserved.
//

#include "Point.h"

Point::Point() {
    x = 0;
    y = 0;
    z = 0;
    r = 0;
    g = 0;
    b = 0;
    radius = 0;
}

Point::Point(float _x, float _y, float _z, float _r, float _g, float _b, float _radius) {
    x = _x;
    y = _y;
    z = _z;
    r = _r;
    g = _g;
    b = _b;
    radius = _radius;
}

Point::~Point() {

}

void Point::draw() {
    glPushMatrix();
    {
        glTranslatef(x, y, z);
        glColor3f(r, g, b);
        glutSolidSphere(radius, 10, 10);
    }
    glPopMatrix();
}
