//
//  Bezier.cpp
//  Bezier Curves
//
//  Created by Eduardo Valencia on 4/12/18.
//  Copyright © 2018 Eduardo Valencia. All rights reserved.
//

#include "Bezier.h"

Bezier::Bezier() {

}

Bezier::Bezier(int deg, Point* points) {
    currentT = 0;
    degree = deg;
    ctrlPoints = new Point[degree];
    for (int i = 0; i < degree; i++) {
        ctrlPoints[i].x = points[i].x;
        ctrlPoints[i].y = points[i].y;
        ctrlPoints[i].z = points[i].z;

        ctrlPoints[i].r = points[i].r;
        ctrlPoints[i].g = points[i].g;
        ctrlPoints[i].b = points[i].b;

        ctrlPoints[i].radius = points[i].radius;
    }

    // Compute coefficientes here
    coefficients = new float[degree];
    for(int i = 0; i < degree; i++) {
        coefficients[i] = binomial(degree - 1, i);
    }
}

Bezier::~Bezier() {

}

long int Bezier::factorial(int x) {
    if (x == 0) {
        return 1;
    } else {
        return(x * factorial(x-1));
    }
}

float Bezier::binomial(int n, int i) {
    return factorial(n)/(factorial(i) * factorial(n-i));
}

Point* Bezier::evalBezier(float t) {
    float* oneMinusT = new float[degree];
    float* tToI = new float[degree];
    for(int i = 0; i < degree; i++) {
        oneMinusT[i] = pow(1-t, degree - 1 - i);
        tToI[i] = pow(t, i);
    }
    float x = 0;
    float y = 0;
    float z = 0;
    for(int i = 0; i < degree; i++) {
        x += coefficients[i] * oneMinusT[i] * tToI[i] * ctrlPoints[i].x;
        y += coefficients[i] * oneMinusT[i] * tToI[i] * ctrlPoints[i].y;
        z += coefficients[i] * oneMinusT[i] * tToI[i] * ctrlPoints[i].z;
    }
    return new Point(x, y, z, 0, 0, 1, 1);
}

void Bezier::draw() {
    //glEnable(GL_COLOR_MATERIAL);
    for(int i = 0; i < degree; i++) {
        //glColor3f(1.0f, 0.0f, 0.0f);
        ctrlPoints[i].draw();
    }
    for(int t = 0; t <= evalPoints; t++) {
        Point* point = evalBezier((float)t/evalPoints);
        point->draw();
    }
    Point* point = evalBezier(currentT);
    point->r = 0;
    point->g = 1;
    point->b = 0;
    point->radius = 0.1f;

    glPushMatrix();
    {
        glTranslatef(point->x, point->y, point->z);
        glColor3f(point->r, point->g, point->b);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glLineWidth(2.5f);
        glBegin(GL_POLYGON);
        for(int i = 0; i < 360; i++) {
            float radians = i * 3.1415f / 180.0f;
            float x = cos(radians) * point->radius;
            float y = -sin(radians) * point->radius;
            glVertex3f(x, y, 0.0f);
        }
        glEnd();
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glLineWidth(1.0f);
    }
    glPopMatrix();
}

void Bezier::update() {
    currentT += 1.0f/200.0f;
    if (currentT >= 1.0f) {
        currentT = 0;
    }
}
