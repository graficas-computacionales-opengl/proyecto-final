//
// Wheel.cpp
//
// David Alexis Zarate Trujillo
// Eduardo Valencia Paz
// César David Betancourt Adame
// Julio
// David
//

#include "Wheel.h"


Wheel::Wheel(float _x, float _y, float _z, GLMmodel*	_wheel) {
  x = _x;
  y = _y;
  z = _z;
  rotationAngleX = 0;
  rotationAngleY = 0;
  wheel = _wheel;
}

Wheel::Wheel(float _x, float _y, float _z) {
  x = _x;
  y = _y;
  z = _z;
  rotationAngleX = 0;
  rotationAngleY = 0;
  wheel = glmReadOBJ("assets/Wheel/Wheel.obj");
  glmUnitize(wheel);
  glmFacetNormals(wheel);
  glmDimensions(wheel, wheel_dims);
}

void Wheel::draw() {
  glPushMatrix();
  {
    glTranslatef(x, y, z);
    glScalef(0.25, 0.185, 0.185);
    glRotatef(90.0f, 0, 1, 0);
    glRotatef(rotationAngleY, 0, 1, 0);
    glRotatef(rotationAngleX, 0, 0, 1);

    /*glPushMatrix();
    {
      glRotatef(rotationAngleX, 0, 0, 1);
    }
    glPopMatrix();*/
    //glRotatef(rotationAngleY, 0, 1, 0);
    glmDraw(wheel, GLM_SMOOTH | GLM_TEXTURE);
    // Load Wheel
    //glutWireSphere(1, 20, 20);
    //rotationAngleX++;
  }
  glPopMatrix();
}

void Wheel::update() {

  if(rotationAngleX > 360.0f) {
    rotationAngleX = 0.0f;
  } else if(rotationAngleX < 0.0f){
    rotationAngleX = 360.0f;
  }
}

void Wheel::rotForward(float angle) {
  rotationAngleX += angle;
}

void Wheel::rotBackwards(float angle) {
  rotationAngleX -= angle;
}

void Wheel::resetWheelSteering() {
  rotationAngleY = 0;
  wheelShouldGoRight = false;
}

void Wheel::rotate(float angle) {
  if(wheelShouldGoRight) {
    rotationAngleY += angle;
  } else {
    rotationAngleY -= angle;
  }
  if(rotationAngleY >= 33.0f) {
    rotationAngleY = 33.0f;
    wheelShouldGoRight = false;
  } else if(rotationAngleY <= -33.0f) {
    wheelShouldGoRight = true;
    rotationAngleY = -33.0f;
  }
}
