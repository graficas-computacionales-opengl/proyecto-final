//
// Wheel.cpp
//
// David Alexis Zarate Trujillo
// Eduardo Valencia Paz
// César David Betancourt Adame
// Julio
// David
//

#include "Building.h"

Building::Building(float _x, float _y, float _z, char* _n, float _s, float _r) {
    building = glmReadOBJ(_n);
    glmUnitize(building);
    glmFacetNormals(building);
    glmDimensions(building, building_dims);

    posX = _x;
    posY = _y;
    posZ = _z;
    size = _s;
    rot = _r;
}

Building::~Building(){

}

void Building::draw(){
    glPushMatrix();
    {
        glTranslatef(posX, posY, posZ);
        glPushMatrix();
        {
            glScalef(size, size, size);
            glRotatef(rot, 0, 1, 0);
            glmDraw(building, GLM_SMOOTH | GLM_TEXTURE);
        }
        glPopMatrix();
    }
    glPopMatrix();
}

void Building::update(){

}
