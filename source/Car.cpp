//
// Wheel.cpp
//
// David Alexis Zarate Trujillo
// Eduardo Valencia Paz
// César David Betancourt Adame
// Julio
// David
//

#include "Car.h"
#include "SimulatedCar.h"

Car::Car(){
  radius = 1.2f;
}

Car::Car(float _x, float _y, float _z, bool _cam) {
    car = glmReadOBJ("assets/Car_1/Car_1.obj");
    glmUnitize(car);
    glmFacetNormals(car);
    glmDimensions(car, car_dims);

    for (int i = 0; i < NUMWHEELS; i++) {
        wheels[i] = new Wheel(0,0,0);
    }
    wheels[0]->x = -0.37;
    wheels[1]->x = -0.37;
    wheels[0]->y = -0.4;
    wheels[1]->y = -0.4;
    wheels[0]->z = 0.65;
    wheels[1]->z = -0.55;
    wheels[2]->x = 0.37;
    wheels[3]->x = 0.37;
    wheels[2]->y = -0.4;
    wheels[3]->y = -0.4;
    wheels[2]->z = 0.65;
    wheels[3]->z = -0.55;
    posX = _x;
    posY = _y;
    posZ = _z;
    rot = 0.0f;
    rotStep = 10.0f;
    vx = 0.0f;
    vz = 1.0f;
    radius = 1.2f;
    shouldMove = true;
    cameraShouldFollow = _cam;
}

Car::~Car(){

}

void Car::moveWithCurve(float _x, float _z) {
  posX = _x;
  posZ = _z;
  printf("Moving\n");
}

void Car::draw(){
    glPushMatrix();
    {
        glTranslatef(posX, posY, posZ);
        glPushMatrix();
        {
            glRotatef(rot, 0, 1.0, 0);
            glScalef(1.5f, 1.5f, 1.5f);
            glmDraw(car, GLM_SMOOTH | GLM_TEXTURE);
            for (int i = 0; i < NUMWHEELS; i++) {
                wheels[i]->draw();
            }
        }
        glPopMatrix();
    }
    glPopMatrix();
}

void Car::update(){
    if(rot > 360){
        rot = rot - 360;
    } else if (rot < 0){
        rot = rot + 360;
    }
    for (int i = 0; i < NUMWHEELS; i++) {
        wheels[i]->update();
    }
    if(cameraShouldFollow){
        glLoadIdentity();
        gluLookAt(
            posX-9.0, posY+17.0, posZ+4.0,
		    posX, posY, posZ,
		    0.0, 1.0, 0.0);
    }
}

void Car::changeCamera(bool _change){
    cameraShouldFollow = _change;
}
void Car::moveCar(float _direction, float _direction2, SimulatedCar* cars[], int carsLength){
    // _direction: 1 -> up, -1 -> down, _direction2: 1 ->right, -1 -> left
  float rotation = rot;
	float mag = 0.15 * _direction2;

	float vx_temp = 0.0f;
	float vz_temp = 1.0f;

	float rotAngle = (rot * M_PI) / 180.0f;
	vx = (vx_temp) * (cos(rotAngle)) + (vz_temp) * (sin(rotAngle));
	vz = (vx_temp) * (-sin(rotAngle)) + (vz_temp) * (cos(rotAngle));

    if(_direction != 0){
        rot += 1.75 * _direction;
		float rotAngleNew = (2 * M_PI) / 180.0f;
		vx = (vx) * (cos(rotAngleNew)) + (vx) * (sin(rotAngleNew));
		vz = (vz) * (-sin(rotAngleNew)) + (vz) * (cos(rotAngleNew));
        if(_direction2 == 1.0f){
            for (int i = 0; i < NUMWHEELS; i++) {
                if(i == 0 || i == 2){
                    wheels[i]->rotate(2.0f);
                }
                wheels[i]->rotForward(20.0f);
            }
        }else if(_direction2 == -1.0f){
            for (int i = 0; i < NUMWHEELS; i++) {
                if(i == 0 || i == 2){
                    wheels[i]->rotate(-2.0f);
                }
                wheels[i]->rotForward(20.0f);
            }
        }
	}else{
        for (int i = 0; i < NUMWHEELS; i++) {
            if(i == 0 || i == 2){
                wheels[i]->resetWheelSteering();
            }
            wheels[i]->rotForward(20.0f);
        }
    }
  futureX = posX + (vx * mag);
	futureZ = posZ + (vz * mag);
  for(int i = 0; i < carsLength; i++) {
    if (willCollide(cars[i])) {
    } else {
      posX = futureX;
      posZ = futureZ;
    }
  }
}

bool Car::willCollide(SimulatedCar* car) {
  float r1 = radius;
  float r2 = car->radius;
  float x1 = futureX;
  float x2 = car->posX;
  float z1 = futureZ;
  float z2 = car->posZ;

  float dx2 = (x2-x1)*(x2-x1);
  float dz2 = (z2-z1)*(z2-z1);
  float dist = dx2 + dz2;
  float sumR = r1 + r2;
  float sumR2 = pow(sumR, 2);
  if(dist < sumR2) {
    return true;
  }
  return false;
}

bool Car::inCollision(SimulatedCar* car) {
  float r1 = radius;
  float r2 = car->radius;
  float x1 = posX;
  float x2 = car->posX;
  float z1 = posZ;
  float z2 = car->posZ;

  float dx2 = (x2-x1)*(x2-x1);
  float dz2 = (z2-z1)*(z2-z1);
  float dist = dx2 + dz2;
  float sumR = r1 + r2;
  float sumR2 = pow(sumR, 2);
  if(dist < sumR2) {
    return true;
  }
  return false;
}
